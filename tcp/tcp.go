package main

import (
	"bufio"
	"flag"
	"fmt"
	"net"
	"os"
)

var (
	host string
	port int
)

func init() {
	flag.StringVar(&host, "host", "127.0.0.1", "redis-server host")
	flag.IntVar(&port, "port", 6379, "redis-server port")
}

func main() {
	flag.Parse()
	redisConnStr := fmt.Sprintf("%s:%d", host, port)
	conn, err := net.Dial("tcp", redisConnStr)
	if err != nil {
		fmt.Printf("Could not connect to Redis at %s\n", redisConnStr)
		os.Exit(1)
	}
	fmt.Fprintf(conn, "*3\r\n$3\r\nSET\r\n$3\r\nFOO\r\n$3\r\nBAR\r\n")
	status, _ := bufio.NewReader(conn).ReadString('\n')
	fmt.Println(status)
}
