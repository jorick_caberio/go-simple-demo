package main

import (
	"bufio"
	"flag"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
)

var (
	host     string
	port     int
	delim    = []byte{'\r', '\n'}
	delimEnd = delim[len(delim)-1]
)

const (
	simpleStrPrefix byte = '+'
	errPrefix       byte = '-'
	intPrefix       byte = ':'
	bulkStrPrefix   byte = '$'
	arrPrefix       byte = '*'
)

func init() {
	flag.StringVar(&host, "host", "127.0.0.1", "redis-server host")
	flag.IntVar(&port, "port", 6379, "redis-server port")
}

// START_main OMIT
func main() {
	flag.Parse()
	redisConnStr := fmt.Sprintf("%s:%d", host, port)
	conn, err := net.Dial("tcp", redisConnStr)
	if err != nil {
		fmt.Printf("Could not connect to Redis at %s\n", redisConnStr)
		os.Exit(1)
	}
	SendCmd("FLUSHALL", conn)
	SendCmd("SET FOO BAR", conn)
	status := RecvCmd(conn)
	fmt.Println(status)
	SendCmd("GET FOO", conn)
	status = RecvCmd(conn)
	fmt.Println(status)
	SendCmd("LPUSH MYLIST a b c d e", conn)
	status = RecvCmd(conn)
	fmt.Println(status)
	SendCmd("LRANGE MYLIST 0 -1", conn)
	status = RecvCmd(conn)
	fmt.Println(status)
}

// END_main OMIT

// START_RecvCmd OMIT
func RecvCmd(conn net.Conn) string {
	r := bufio.NewReader(conn)
	return bufioReadResp(r)
}

// END_RecvCmd OMIT

// START_bufioReadResp OMIT
func bufioReadResp(r *bufio.Reader) string {
	b, err := r.Peek(1)
	if err != nil {
		return "nil"
	}
	switch b[0] {
	case simpleStrPrefix, errPrefix, intPrefix:
		return readSimpleStr(r)
	case bulkStrPrefix:
		return readBulkStr(r)
	case arrPrefix:
		return readArray(r)
	default:
		return "I have no idea what i'm doing"
	}
}

// END_bufioReadResp OMIT

func readSimpleStr(r *bufio.Reader) string {
	b, _ := r.ReadBytes(delimEnd)
	return string(b[1 : len(b)-2])
}

func readBulkStr(r *bufio.Reader) string {
	b, _ := r.ReadBytes(delimEnd)
	size, _ := strconv.ParseInt(string(b[1:len(b)-2]), 10, 64)

	if size < 0 {
		return "nil"
	}
	total := make([]byte, size)
	b2 := total
	var n int
	for len(b2) > 0 {
		n, _ = r.Read(b2)
		b2 = b2[n:]
	}

	// There's a hanging \r\n there, gotta read past it
	trail := make([]byte, 2)
	for i := 0; i < 2; i++ {
		c, _ := r.ReadByte()
		trail[i] = c

	}
	blens := len(b) + len(total)
	raw := make([]byte, 0, blens+2)
	raw = append(raw, b...)
	raw = append(raw, total...)
	raw = append(raw, trail...)
	return string(total)
}

func readArray(r *bufio.Reader) string {
	b, _ := r.ReadBytes(delimEnd)

	size, _ := strconv.Atoi(string(b[1 : len(b)-2]))
	if size <= 0 {
		return "nil"
	}
	arr := ""
	for i := 0; i < size; i++ {
		m := bufioReadResp(r)
		m += "\n"
		arr += m

	}
	return string(arr[:len(arr)-1])
}

func SendCmd(line string, conn net.Conn) {
	const (
		arrayPrefix      = "*"
		bulkStringPrefix = "$"
		lineEnding       = "\r\n"
	)
	cmdStr := ""
	args := strings.Fields(line)

	cmdStr += arrayPrefix
	cmdStr += strconv.Itoa(len(args))
	cmdStr += lineEnding

	for _, arg := range args {
		cmdStr += bulkStringPrefix
		cmdStr += strconv.Itoa(len(arg))
		cmdStr += lineEnding
		cmdStr += arg
		cmdStr += lineEnding
	}

	fmt.Fprint(conn, cmdStr)
}
