package main

import (
	"flag"
	"fmt"
)

var (
	host string
	port int
)

func init() {
	flag.StringVar(&host, "host", "127.0.0.1", "redis-server host")
	flag.IntVar(&port, "port", 6379, "redis-server port")
}

func main() {
	flag.Parse()
	redisConnStr := fmt.Sprintf("%s:%d", host, port)
	fmt.Println(redisConnStr)
}
