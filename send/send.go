package main

import (
	"bufio"
	"flag"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
)

var (
	host string
	port int
)

func init() {
	flag.StringVar(&host, "host", "127.0.0.1", "redis-server host")
	flag.IntVar(&port, "port", 6379, "redis-server port")
}

// START_SendCmd OMIT
func SendCmd(line string, conn net.Conn) {
	const (
		arrayPrefix      = "*"
		bulkStringPrefix = "$"
		lineEnding       = "\r\n"
	)
	cmdStr := ""
	args := strings.Fields(line)
	cmdStr += arrayPrefix
	cmdStr += strconv.Itoa(len(args))
	cmdStr += lineEnding
	for _, arg := range args {
		cmdStr += bulkStringPrefix
		cmdStr += strconv.Itoa(len(arg))
		cmdStr += lineEnding
		cmdStr += arg
		cmdStr += lineEnding
	}
	fmt.Fprint(conn, cmdStr)
}

// END_SendCmd OMIT

// START_main OMIT
func main() {
	flag.Parse()
	redisConnStr := fmt.Sprintf("%s:%d", host, port)
	conn, err := net.Dial("tcp", redisConnStr)
	if err != nil {
		fmt.Printf("Could not connect to Redis at %s\n", redisConnStr)
		os.Exit(1)
	}
	SendCmd("SET FOO BAR", conn)
	status, _ := bufio.NewReader(conn).ReadString('\n')
	fmt.Println(status)
}

// END_main OMIT
